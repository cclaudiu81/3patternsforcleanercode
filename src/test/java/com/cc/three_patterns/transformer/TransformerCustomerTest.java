package com.cc.three_patterns.transformer;

import com.cc.three_patterns.domainobjects.*;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class TransformerCustomerTest {

    private static final int ZERO = 0;
    private static final int ONE = 1;

    @Test
    public void transform_should_return_empty_vendors_for_empty_customers() {
        final Customers customers = getMockedCustomers(ZERO);

        TransformerService transformerService = TransformerService.getInstance();

        final Vendors vendors = transformerService.transform(customers, new PredicateTransformer<Customer, Vendor>() {

            /** Domain Logic deferred to Client */
            @Override
            public void map(Customer customer, Vendor vendor) {
                vendor.setName(customer.getName());
                vendor.setAddress(customer.getAddress());
            }
        });

        assertThat(vendors.isEmpty(), is(true));
    }

    @Test
    public void transform_should_return_one_vendor_for_one_customer() {
        final Customers customers = getMockedCustomers(ONE);

        TransformerService transformerService = TransformerService.getInstance();

        final Vendors vendors = transformerService.transform(customers, new PredicateTransformer<Customer, Vendor>() {

             /** Domain Logic deferred to Client */
            @Override
            public void map(Customer customer, Vendor vendor) {
                vendor.setName(customer.getName());
                vendor.setAddress(customer.getAddress());
            }
        });

        assertThat(vendors.getContainerSize(), is(ONE));
        assertThat(vendors.lookupVendor(ZERO).getVendorName(), is(equalTo(customers.getIterator().next().getName())));
    }

    private Customers getMockedCustomers(int customerSize) {
        final Customers customers = new RetailCustomers();

        if(customerSize == 1) {
            customers.addCustomer(Customer.newInstance("new_customer", 32, Address.newInstance("work_address", "home_address")));
        }

        return customers;
    }
}
