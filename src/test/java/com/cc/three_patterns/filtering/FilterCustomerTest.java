package com.cc.three_patterns.filtering;

import com.cc.three_patterns.domainobjects.Customer;
import com.cc.three_patterns.domainobjects.Customers;
import com.cc.three_patterns.domainobjects.RetailCustomers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class FilterCustomerTest {

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TEN = 10;

    private FilterCustomerService filterService;
    private Customers customerContainer;

    @Before
    public void setUp() {
        filterService = new FilterCustomerService();
        customerContainer = new RetailCustomers();
    }

    @After
    public void tearDown() {
        // explicitly de-allocate resources(a matter of readability rather performance...somehow redundant)
        filterService = null;
        customerContainer = null;
    }

    @Test
    public void filter_customers_should_return_empty_container() {
        ((RetailCustomers)(customerContainer)).mock(ZERO);

        final Customers customersAboveThirty = filterService.filter(customerContainer, new FilterPredicate() {
            @Override
            public boolean evaluate(Customer customer) {
                return customer.getAge() > 30;
            }
        });

        assertThat(customersAboveThirty.getContainerSize(), is(ZERO));
    }

    @Test
    public void filter_customers_should_return_a_single_customer() {
        ((RetailCustomers) customerContainer).mock(TEN);

        final Customers oneFilteredCustomer = filterService.filter(customerContainer, new FilterPredicate() {
            @Override
            public boolean evaluate(Customer customer) {
                return customer.getAge() == 10;
            }
        });

        assertThat(oneFilteredCustomer.getContainerSize(), is(ONE));
    }
}