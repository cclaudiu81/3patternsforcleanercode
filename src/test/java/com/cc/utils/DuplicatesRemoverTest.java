package com.cc.utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class DuplicatesRemoverTest {

    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;

    @Test
    public void removeDuplicates_empty_container_return_empty_container_back() {
        final List<String> empty_container = Collections.emptyList();

        final List<String> processedContainer = DuplicatesRemover.removeDuplicates(empty_container);

        assertThat(processedContainer.isEmpty(), is(true));
    }

    @Test
    public void removeDuplicates_container_with_two_duplicates_return_one_element_container() {
        final List<String> duplicateElementContainer = Arrays.asList("oneDuplicate", "oneDuplicate");

        final List<String> processedContainer = DuplicatesRemover.removeDuplicates(duplicateElementContainer);

        assertThat(processedContainer.size(), is(ONE));
    }

    @Test
    public void removeDuplicates_container_with_three_duplicates_return_two_element_container() {
        final List<String> duplicateElementContainer = Arrays.asList("secondWhichIsNotDuplicate", "oneDuplicate", "oneDuplicate", "oneDuplicate");

        final List<String> processedContainer = DuplicatesRemover.removeDuplicates(duplicateElementContainer);

        assertThat(processedContainer.size(), is(TWO));
    }

    @Test
    public void removeDuplicates_container_with_three_random_duplicates_return_container_with_three_elements() {
        final List<String> duplicateElementContainer = Arrays.asList("second", "one", "second", "third", "second");

        final List<String> processedContainer = DuplicatesRemover.removeDuplicates(duplicateElementContainer);

        assertThat(processedContainer.size(), is(THREE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeDuplicates_nullContainer_throw_fail_fast_exception() {
        final List<String> nullContainer = null;

        DuplicatesRemover.removeDuplicates(nullContainer);
    }
}
