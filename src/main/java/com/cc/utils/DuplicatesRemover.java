package com.cc.utils;

import com.cc.three_patterns.dbc.ContractAssert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class DuplicatesRemover {

    private DuplicatesRemover() {
    }

    public static <T extends Comparable<T>> List<T> removeDuplicates(Collection<T> duplicates) {
        ContractAssert.notNull(duplicates, "Duplicate Element Container cannot be null!");

        if (duplicates.isEmpty()) {
            return Collections.emptyList();
        }

        final List<T> uniqueElements = new ArrayList<>();
        for(T each: duplicates) {
            if(! uniqueElements.contains(each)) {
                uniqueElements.add(each);
            }
        }
        return uniqueElements;
    }
}
