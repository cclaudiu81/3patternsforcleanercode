package com.cc.three_patterns.domainobjects;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class Vendor {
    private String vendorName;
    private Address vendorAddress;

    public static Vendor newInstance() {
        return new Vendor();
    }

    public String getVendorName() {
        return vendorName;
    }

    public Address getVendorAddress() {
        return vendorAddress;
    }

    public void setName(String name) {
        this.vendorName = name;
    }

    public void setAddress(Address address) {
        this.vendorAddress = address;
    }

    @Override
    public String toString() {
        return "{ Name: " + getVendorName() + " ," + "Address: " + getVendorAddress() + " }";
    }
}
