package com.cc.three_patterns.domainobjects;

import com.cc.three_patterns.dbc.ContractAssert;
import com.google.common.collect.Iterators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class Vendors {
    private final List<Vendor> vendors = new ArrayList<>();

    private Vendors() { }

    public static Vendors newInstance() {
        return new Vendors();
    }

    public void addVendor(Vendor vendor) {
        vendors.add(vendor);
    }

    public Vendor lookupVendor(int index) {
        ContractAssert.isTrue(index >= 0, "Vendor Container Index should be equal or higher than 0!");
        ContractAssert.isTrue(vendors.size() > index, "Vendor Index is out of bounds!");

        final Vendor lookupVendor = vendors.get(index);
        ContractAssert.notNull(lookupVendor, "PostCondition violated! Vendor at the lookedUp position is null!");

        return lookupVendor;
    }

    public Iterator<Vendor> getIterator() {
        return vendors.iterator();
    }

    public int getContainerSize() {
        return vendors.size();
    }

    public Boolean isEmpty() {
       return vendors.isEmpty();
    }
}
