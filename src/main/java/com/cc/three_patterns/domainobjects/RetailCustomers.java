package com.cc.three_patterns.domainobjects;

import com.cc.three_patterns.dbc.ContractAssert;

import java.util.Iterator;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class RetailCustomers extends Customers {

    @Override
    public int getContainerSize() {
        return customerContainer.size();
    }

    @Override
    public Customer lookupCustomer(int position) {
        ContractAssert.isTrue(position >= 0, "Customer Container Index should be equal or higher than 0!");
        ContractAssert.isTrue(customerContainer.size() > position, "Customer Index is out of bounds!");

        final Customer lookupCustomer = customerContainer.get(position);
        ContractAssert.notNull(lookupCustomer, "PostCondition violated! Customer at the lookedUp position is null!");

        return lookupCustomer;
    }

    @Override
    public void addCustomer(Customer each) {
        ContractAssert.notNull(each, "Cannot add a null customer!");

        final int preSize = getContainerSize();

        customerContainer.add(each);
        // some code for adding algorithm...

        final int postSize = getContainerSize();

        ContractAssert.isTrue(preSize + 1 == postSize, "PostCondition Violated! AddCustomer() routine failed adding a customer!");
    }

    @Override
    public Iterator<Customer> getIterator() {
        return customerContainer.iterator();
    }

    /*
     * non-javadoc
     *
     * more like an utility routine for our DEMO on TDD
     */
    public void mock(int number) {
        for (int idx = 0; idx < number; ++idx) {
            addCustomer(Customer.newInstance("customer_name" + idx, idx + 10, Address.newInstance("workAddress_" + idx, "home_address_" + idx)));
        }
    }
}
