package com.cc.three_patterns.domainobjects;

import com.cc.three_patterns.dbc.ContractAssert;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class Customer {
    private final String name;
    private final int age;
    private Address address;

    private Customer(String name, int age, Address address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public static Customer newInstance(String name, int age, Address address) {
        ContractAssert.notNull(name, "Invariant Violated! Name should not be null!");
        ContractAssert.notNull(age, "Invariant Violated! Age should not be null!");
        ContractAssert.isTrue(age > 0, "Invariant Violated! Age should be higher than 0!");
        ContractAssert.notNull(address, "Customer has to live somewhere!Address cannot be null!");

        return new Customer(name, age, address);
    }

    public int getAge() {
        ContractAssert.isTrue(age > 0, "Invariant Violated! Age should be higher than 0!");
        return age;
    }

    public String getName() {
        ContractAssert.notNull(name, "Invariant violated! Name cannot be null! Accessing instance through reflection is forbidden!");
        return name;
    }

    public Address getAddress() {
        return address;
    }
}
