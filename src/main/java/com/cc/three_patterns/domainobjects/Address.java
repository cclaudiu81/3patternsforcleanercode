package com.cc.three_patterns.domainobjects;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public class Address {
    private final String workAddress;
    private final String homeAddress;

    private Address(String workAddress, String homeAddress) {
        this.workAddress = workAddress;
        this.homeAddress = homeAddress;
    }

    public static Address newInstance(String workAddress, String homeAddress) {
        return new Address(workAddress, homeAddress);
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    @Override
    public String toString() {
        return "{ Work Address: " + getWorkAddress() + ", " + "Home Address: " + getHomeAddress() + " }";
    }
}
