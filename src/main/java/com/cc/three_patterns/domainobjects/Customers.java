package com.cc.three_patterns.domainobjects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public abstract class Customers {

    protected final List<Customer> customerContainer = new ArrayList<Customer>();

    public abstract int getContainerSize();

    public abstract Customer lookupCustomer(int position);

    public abstract void addCustomer(Customer each);

    public abstract Iterator<Customer> getIterator();
}
