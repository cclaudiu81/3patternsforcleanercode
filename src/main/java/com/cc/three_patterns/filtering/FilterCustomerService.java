package com.cc.three_patterns.filtering;

import com.cc.three_patterns.dbc.ContractAssert;
import com.cc.three_patterns.domainobjects.Customer;
import com.cc.three_patterns.domainobjects.Customers;
import com.cc.three_patterns.domainobjects.RetailCustomers;

import java.util.Iterator;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 *
 * This approach uses the inversion of control methodology by hoisting(lifting-up) the responsibility
 * of passing an instance of the {@link FilterPredicate} to the caller.
 * This mechanic helps also to adhere to: "code to interfaces rather than implementations!" OO Principle.
 * Using polymorphism we can pass any instance of the {@link FilterPredicate} Interface...(in fact we defer
 * this responsibility to the caller, by implementing the ONLY one routine--declared as part of the public-exported api)
 * which is {code evaluate()}
 *
 * author cclaudiu
 */
public class FilterCustomerService {

    public Customers filter(Customers customerContainer, FilterPredicate filterPredicate) {
        ContractAssert.notNull(customerContainer, "Precondition Violated! Customer Container should not be null!");
        ContractAssert.notNull(filterPredicate, "Precondition Violated! A filter should not be null in order for filter to work properly!");

        final Customers customers = new RetailCustomers();
        for(final Iterator<Customer> customerIterator = customerContainer.getIterator(); customerIterator.hasNext(); ) {
            final Customer each = customerIterator.next();

            if(filterPredicate.evaluate(each)) {
                customers.addCustomer(each);
            }
        }

        ContractAssert.isTrue(customers.getContainerSize() >= 0, "Postcondition Violated! Filtered Customers should be >= 0");

        return customers;
    }
}
