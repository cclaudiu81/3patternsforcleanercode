package com.cc.three_patterns.filtering;

import com.cc.three_patterns.domainobjects.Customer;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public interface FilterPredicate {
    boolean evaluate(Customer customer);
}
