package com.cc.three_patterns.dbc;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * <p/>
 * author cclaudiu
 */
public final class ContractAssert {

    private ContractAssert() {
    }

    public static final void notNull(Object that, String message) {
        if(that == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static final void isTrue(Boolean isTrue, String message) {
        if(!isTrue) {
            throw new IllegalArgumentException(message);
        }
    }
}
