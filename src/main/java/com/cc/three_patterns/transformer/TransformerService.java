package com.cc.three_patterns.transformer;

import com.cc.three_patterns.dbc.ContractAssert;
import com.cc.three_patterns.domainobjects.Customer;
import com.cc.three_patterns.domainobjects.Customers;
import com.cc.three_patterns.domainobjects.Vendor;
import com.cc.three_patterns.domainobjects.Vendors;

import java.util.Iterator;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public abstract class TransformerService {

    /**
     * Encapsulate the instance returned to the Client, by wrapping the instance creation inside this static factory
     * method...
     */
    public static TransformerService getInstance() {
        return new CustomerToVendorTransformation();
    }

    public abstract Vendors transform(Customers customers, PredicateTransformer<Customer, Vendor> predicateTransformer);

    private static class CustomerToVendorTransformation extends TransformerService {

        /**
         * Defer the domain logic mapping to the Client, and encapsulate the transformation in this Service Class
         * Let the transform routine handle the iteration & DesignByContract Concerns & Returned Container creation
         */
        @Override
        public Vendors transform(Customers customers, PredicateTransformer<Customer, Vendor> predicateTransformer) {
            ContractAssert.notNull(customers, "Customers Container cannot be null!");

            final Vendors transformedVendors = Vendors.newInstance();

            for(final Iterator<Customer> customerIterator = customers.getIterator(); customerIterator.hasNext(); ) {
                transformedVendors.addVendor(getMappedVendor(customerIterator.next(), predicateTransformer));
            }

            ContractAssert.isTrue(customers.getContainerSize() == transformedVendors.getContainerSize(),
                    "PostCondition Violated! Vendor Container should be of the same size as the Customer Container!");

            return transformedVendors;
        }

        private Vendor getMappedVendor(Customer eachCustomer, PredicateTransformer<Customer, Vendor> predicateTransformer) {
            final Vendor vendor = Vendor.newInstance();

            // domain logic concern -> take out this responsibility from the transform routine
            predicateTransformer.map(eachCustomer, vendor);

            return vendor;
        }
    }
}
