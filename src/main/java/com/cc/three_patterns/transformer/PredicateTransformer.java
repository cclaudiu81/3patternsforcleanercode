package com.cc.three_patterns.transformer;

/**
 * "I am not a great programmer, i am just a good programmer with great habits"
 * [K.Beck]
 * author cclaudiu
 */
public interface PredicateTransformer<T, V> {

    /**
     * Map the Source: T, to destination: V
     * The downside of this solution is that the T & V should refer to mutable Classes
     * The Client is responsible of mapping the Domain Objects, after their instances are created...
     */
    void map(T customer, V vendor);
}
